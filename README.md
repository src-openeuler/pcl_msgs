# pcl_msgs

#### 介绍
包含与PCL（点云库）相关的ROS消息的软件包。

#### 软件架构
软件架构说明

包含与PCL（点云库）相关的ROS消息的msg和srv

文件内容:
```
pcl_msgs/
├── CHANGELOG.rst
├── CMakeLists.txt
├── msg
│   ├── ModelCoefficients.msg
│   ├── PointIndices.msg
│   ├── PolygonMesh.msg
│   └── Vertices.msg
├── package.xml
├── README.md
└── srv
    └── UpdateFilename.srv
```

#### 安装教程

1. 下载rpm包

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-pcl_msgs/ros-noetic-ros-pcl_msgs-0.3.0-1.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-pcl_msgs/ros-noetic-ros-pcl_msgs-0.3.0-1.oe2203.x86_64.rpm 

2. 安装rpm包

aarch64:

sudo rpm -ivh ros-noetic-ros-pcl_msgs-0.3.0-1.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-pcl_msgs-0.3.0-1.oe2203.x86_64.rpm --nodeps --force

#### 使用说明

依赖环境安装:

sh /opt/ros/noetic/install_dependence.sh

安装完成以后，在/opt/ros/noetic/目录下有如下输出,则表示安装成功

输出:
```
pcl_msgs/
├── cmake.lock
├── env.sh
├── include
│   └── pcl_msgs
│       ├── ModelCoefficients.h
│       ├── PointIndices.h
│       ├── PolygonMesh.h
│       ├── UpdateFilename.h
│       ├── UpdateFilenameRequest.h
│       ├── UpdateFilenameResponse.h
│       └── Vertices.h
├── lib
│   ├── pkgconfig
│   │   └── pcl_msgs.pc
│   └── python2.7
│       └── dist-packages
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    ├── common-lisp
    │   └── ros
    ├── gennodejs
    │   └── ros
    ├── pcl_msgs
    │   └── cmake
    └── roseus
        └── ros

```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
