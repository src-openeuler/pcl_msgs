# pcl_msgs

#### Description
Package containing PCL (Point Cloud Library)-related ROS messages.

#### Software Architecture
Software architecture description

Package containing PCL (Point Cloud Library)-related msgs and srvs

input:
```
pcl_msgs/
├── CHANGELOG.rst
├── CMakeLists.txt
├── msg
│   ├── ModelCoefficients.msg
│   ├── PointIndices.msg
│   ├── PolygonMesh.msg
│   └── Vertices.msg
├── package.xml
├── README.md
└── srv
    └── UpdateFilename.srv

```

#### Installation

1.  Download RPM

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-pcl_msgs/ros-noetic-ros-pcl_msgs-0.3.0-1.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-pcl_msgs/ros-noetic-ros-pcl_msgs-0.3.0-1.oe2203.x86_64.rpm 

2.  Install RPM

aarch64:

sudo rpm -ivh ros-noetic-ros-pcl_msgs-0.3.0-1.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-pcl_msgs-0.3.0-1.oe2203.x86_64.rpm --nodeps --force

#### Instructions


Dependence installation

sh /opt/ros/noetic/install_dependence.sh

Exit the following output file under the /opt/ros/noetic/ directory , Prove that the software installation is successful

```
pcl_msgs/
├── cmake.lock
├── env.sh
├── include
│   └── pcl_msgs
│       ├── ModelCoefficients.h
│       ├── PointIndices.h
│       ├── PolygonMesh.h
│       ├── UpdateFilename.h
│       ├── UpdateFilenameRequest.h
│       ├── UpdateFilenameResponse.h
│       └── Vertices.h
├── lib
│   ├── pkgconfig
│   │   └── pcl_msgs.pc
│   └── python2.7
│       └── dist-packages
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    ├── common-lisp
    │   └── ros
    ├── gennodejs
    │   └── ros
    ├── pcl_msgs
    │   └── cmake
    └── roseus
        └── ros

```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
